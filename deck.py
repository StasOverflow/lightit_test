import random


SUITS = ["♠", "♥", "♦", "♣"]
RANKS = ["A", "2", "3", "4", "5", "6", "7", 
         "8", "9", "10", "J", "Q", "K"]


class Card:
    """A class, to hold value and suit of a card for a card game
    
    Also may use a specific mapping of card suits for unusual cases
    (eg. a card game durak, where Ace's value(rank) is more then King's)

    Attributes:
        value (int): value (rank) of a card.
        suit  (int): a value, representing suit of card, (can be used 
             as a cell of SUITS array for output)
        rank_name (str): a string, containing rank of a card to print.

    """ 
    def __init__(self, suit, value, rank_name = None):
        """A method to create an object of a Card class.

        Sets properties of a card, like suit and value(rank), but
        differs from typical card init method, containing 1 bonus
        argument, rank_name to specify a name of a card, if necessary

        Note:
            If no rank name argument were given, then the 
            name of a card is specified, using cards value as a cell of
            RANKS array

        Args:
            value (int): a value (rank) of a card
            suit  (int): an int value, representing suit of a card
            rank_name (str): specifies a name of the card (default None)
        """
        self.suit = suit
        self.value = value
        if rank_name is not None:
            self.rank_name = rank_name
        else:
            self.rank_name = RANKS[self.value]

    def __str__(self):
        """A method to output card in human readable format

        Example:
            card = Card(1, 3)
            print(card)
            
            Output:
                4♥
        """
        return (self.rank_name + SUITS[self.suit])


class Deck:
    """Forms a set of cards into a deck for a card game
    
    Also may use a specific mapping of card suits for unusual cases
    (eg. a card game durak, where Ace's value(rank) is more then Kings)

    Attributes:
        cards (:obj:`list` of :obj:'Card'): an array of Cards left in
            the current instance of Deck
    """ 
    def __init__(self, deck_size = 52, deck_specific_ranks = None):
        self.cards = []
        card_num = deck_size >> 2    # number of cards of each suit
        suit_count = 4

        for suit in range(suit_count):
            for value in range(card_num):
                if deck_specific_ranks is not None:
                    self.cards.append(Card(suit, value,
                                           deck_specific_ranks[value]))
                else:
                    self.cards.append(Card(suit, value))

    def __str__(self):
        """Prints out all cards left on the deck (kinda ugly for now)"""
        s = ""
        if self.cards_left() is not 0:
            for i in range(len(self.cards)):
                s = s + " " * i + str(self.cards[i]) + "\n"
            return s
        else:
            return 'No cards in the deck left'

    def cards_left(self):
        """Returns a number of cards left in the deck"""
        return len(self.cards)

    def shuffle(self):
        """Replaces current deck with shuffled one"""
        deck = []
        deck_size = self.cards_left()

        while len(deck) < deck_size:
            random_card_index = random.randrange(0, len(self.cards))
            deck.append(self.cards[random_card_index])
            del self.cards[random_card_index]
        self.cards = deck

    def card_get(self, random = False):
        """deals card from a deck

        Args:
            random (int): a flag, which indicates what card should be
                removed from a deck, either random (if true) or top from
                the top of the deck. Defaults to None

        Returns:
            card (:obj:'Card'): an instance of Card, removed from card list
        """
        card = self.cards.pop()
        return card

    def size_get(self):
        """Returns a value of cards, left in the deck"""
        return len(self.cards)

