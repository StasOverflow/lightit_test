from deck import Deck
import random
import copy


def deck_select():
    """An input function to determine a size of a deck to play with

    Returns:
        deck_size (int): either 36 or 52 (default deck size for durak
                    game, others are not yet supported)
    """
    while True:
        print("-------------------------------------------\n"
              + "Enter amount of cards to play with \n"
              + "-> 36 cards deck (recommended) \n"
              + "-> 52 cards deck \n"
              + "-------------------------------------------\n")
        deck_size = input()        
        try:
            deck_size = int(deck_type)
            if (deck_size == 36 or deck_size == 52):
                print("\n\nyou have chosen a "
                      + str(deck_type)
                      + "- card" + " deck")
                return deck_size
            else:
                raise
        except:
            print("unrecognized deck type, pls choose another one")


class DurakDeck(Deck):
    """Class to make usual deck suitable for durak game

    Args:
        deck_size (int): specifier of a set of cards to use in a game
                            either with 2,3,4,5 (52 case), or without.
                            Defaults to 36
    """
    def __init__(self, deck_size = 36):
        if deck_size == 36:
            durak_ranks = ["6", "7", "8", "9", "10", 
                           "J", "Q", "K", "A"]
        elif deck_size == 52:
            durak_ranks = ["2", "3", "4", "5", "6", "7", "8", 
                           "9", "10", "J", "Q", "K", "A"]
        Deck.__init__(self, deck_size, durak_ranks)


class Player:
    """A class to represent a player for durak game

    For now, player can only draw cards

    Attributes:
        hand (:obj:`list` of :obj:'Card'): an array of Cards in hand
                of a certain player
    """
    def __init__(self):
        """inits a 'hand' for a player"""
        self.hand = []
        pass

    def __str__(self):
        """prints out cards in player's hand"""
        s = "|"
        for i in range(len(self.hand)):
            offset = 3 - len(str(self.hand[i].rank_name))
            s = s + " "*offset + str(self.hand[i]) + " " + "|"
        s = s + "\n"
        return s

    def draw(self, card):
        """take a card (add it to hand)

        Arguments:
            card (:obj:'Card')
        """
        self.hand.append(card)

    def play(self, index):
        """Removes a card from hand

        Args:
            index (int): and index of card in hand to remove
        Returns:
            card (:obj:'Card')
        """
        card = self.hand.pop(index)
        return card

    def how_many(self):
        """returns number of cards left in hand"""
        return len(self.hand)


class Durak_session:
    """A session of a durak game

    Attributes:
        trump (int): a suit of a trump card, chosen for this session
        player_quan(int) : quantity of players to participate in a 
                current game session
        players (:obj:list of :obj:'Player'): players, participating
                current game session
    """
    trump = None
    player_quan = None
    __deck = None   # an instance of Deck

    def __init__(self, cards_to_play = 36):
        """starts a session of Durak

        Creates a deck, shuffles it, applies a number of players in a
        current game session, and deals cards to them.

        Arguments:
            cards_to_play (int): basically a representation of a deck
                (36 or 52 cards variants)
        """
        self.__start(cards_to_play)

    def __start(self, cards_to_play):
        self.__deck = DurakDeck(cards_to_play)
        self.__deck.shuffle();
        self.__player_quan_insert(cards_to_play)
        self.__first_deal()

    def __trump_select(self, overdraw = False):
        self.__trump = self.__deck.card_get(True)
        if overdraw:
            place_to_insert = random.randrange(0, len(self.__deck.cards))
            self.__deck.cards.insert(place_to_insert, self.__trump)
        else:
            self.__deck.cards.insert(0, self.__trump)

    def __player_quan_insert(self, cards_to_play = 36):
        self.players = []
        max_players = 5 if cards_to_play == 36 else 8
        while self.player_quan is None:
            print("-------------------------------------------\n"
                  + " Enter a number of players to participate\n"
                  + (" from 2 to " + str(max_players) + "\n")
                  + "-------------------------------------------\n")
            player_quan = input()
            try:
                player_quan = int(player_quan)
                if player_quan < 2:
                    print("there cannot be less then two players")
                elif player_quan == 6 and player_quan == 36:
                    print("are u sure u want 6 players to continue?"
                          + "there won't be no talon!")
                elif player_quan > max_players:
                    print("too many players, there won't be enough cards!")
                else:
                    print("Starting a game")
                    for i in range(player_quan):
                        self.players.append(Player())

                    self.player_quan = player_quan

            except:
                print('wrong number, try again')

    def __first_deal(self):
        cards_per_player = 6
        overdraw = False
        if self.player_quan == 6 and len(self.__deck.cards == 36):
            overdraw = True
        self.__trump_select(overdraw)

        for i in range(cards_per_player):
            for j in range(self.player_quan):
                self.players[j].draw(self.card_deal())
        
        print("Trump card: ", str(self.__trump))
        for i in range(self.player_quan):
            print("Player " + str(i+1) + " deck is:\n"
                  + str(self.players[i]))

    def find_strongest(self, suitwise = False):
        """Find out which set of cards is the strongest in a current game
        
        Provides with two possible algorithms of comparison:
            Suitless: to compare cards only rank-wise
            Suitwise: to compare cards, taking their suits in account
        
        Note:
            Suitwise algorithm is more sane in terms of durak game flow, but
            using suitless algorithm provides more good-looking output.

        Suggestion: 
            Set sutwise to True, and compare outputs.

        Arguments:
            trump (int): a suit of a trump from current game
            suitwise (boolean): indicates which algorithm of comparison to
                    choose from. Defaults to False
        """
        cards_beaten = []
        trump = self.trump
        for x in range(self.player_quan):
            player_list = copy.deepcopy(self.players)
            player = player_list.pop(x)
            cards_in_hand = player.how_many()
            card_power = 0

            for y in range(cards_in_hand):
                card = player.hand.pop(0)
                for i in range(len(player_list)):
                    for other_card in (player_list[i].hand):
                        if other_card.suit == trump:
                            other_card.value = other_card.value + 9
                        if card.suit == trump:
                            card.value = card.value + 9 
                        if suitwise:
                            if card.suit == other_card.suit or card.suit == trump:
                                if card.value - other_card.value > 0:
                                    card_power = card_power + 1
                        else:
                            if card.value - other_card.value > 0:       
                                card_power = card_power + 1
            cards_beaten.append(card_power)

        maximum = max(cards_beaten)
        num = []
        for i in range(len(cards_beaten)):
            if maximum == cards_beaten[i]:
                num.append(i)
        s = ""
        if len(num) > 1:
            s = "players "
            for x in range(len(num)):
                addition = "and " if x != len(num) else ""
                s = s + str(num[x] + 1) + " " + addition
            s = s + "have equally strong set of cards in their hands"
        else:
            s = "player " + str(num[0] + 1) + " has the strongest set of cards"
        print(s)
            
    def trump_get(self):
        """Returns suit of a current game's trump"""
        return self.__trump.suit

    def card_deal(self):
        """Deals a card from top of the deck"""
        if self.__deck.cards_left() > 0:
            card = self.__deck.card_get()
            return card
        else:
            print("NO CARDS LEFT, AMIGO")
            return None